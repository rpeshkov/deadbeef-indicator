DeadBeef appindicator
=====================

Simple deadbeef appindicator.


Usage:
======

With git:

    git clone https://github.com/rpeshkov/deadbeef-indicator.git
    cd deadbeef-indicator/
    chmod +x ./deadbeef-indicator
    ./deadbeef-indicator

Without git:

    mkdir ~/bin
    cd ~/bin
    wget https://raw.githubusercontent.com/rpeshkov/deadbeef-indicator/master/deadbeef-indicator
    chmod +x ./deadbeef-indicator
    ./deadbeef-indicator
